"""Testing limit_order_book.LimitOrderBook in src/models/limit_order_book.py.

Primarily testing 'reduce_orders' method as that is where the business logic primarily resides.

Methods are separated because if a test fails, remaining assertions within same method are not run.
"""

from src.models import limit_order_book
from src.models import order


def test_reduce_orders_maximizes_result_if_maximize_is_true():
    """Asserts that reduce_orders() maximizes the result if maximize=True.
    """
    target_size = 200
    file_output_path = None
    ledger = limit_order_book.LimitOrderBook(target_size, file_output_path)

    order_1 = order.Order('a', '28800538', order.OrderType.buy, 44.10, 100)
    order_2 = order.Order('b', '28800539', order.OrderType.buy, 43.95, 100)
    order_3 = order.Order('c', '28800540', order.OrderType.buy, 58.93, 100)

    orders = [
        order_1,
        order_2,
        order_3
    ]

    amount = ledger.reduce_orders(orders, maximize=True)
    assert amount == 58.93 * 100 + 44.10 * 100


def test_reduce_orders_minimizes_result_if_maximize_is_false():
    """Asserts that reduce_orders() minimizes the result if maximize=False.
    """
    target_size = 200
    file_output_path = None
    ledger = limit_order_book.LimitOrderBook(target_size, file_output_path)

    order_1 = order.Order('a', '28800538', order.OrderType.buy, 44.10, 100)
    order_2 = order.Order('b', '28800539', order.OrderType.buy, 43.95, 100)
    order_3 = order.Order('c', '28800540', order.OrderType.buy, 58.93, 100)

    orders = [
        order_1,
        order_2,
        order_3
    ]

    amount = ledger.reduce_orders(orders, maximize=False)
    assert amount == 43.95 * 100 + 44.10 * 100


def test_reduce_orders_returns_total_if_target_size_and_total_shares_match():
    """Asserts that reduce_orders() returns the total value of the orders if the target size and the total number of shares match.
    """
    target_size = 300
    file_output_path = None
    ledger = limit_order_book.LimitOrderBook(target_size, file_output_path)

    order_1 = order.Order('a', '28800538', order.OrderType.buy, 44.10, 100)
    order_2 = order.Order('b', '28800539', order.OrderType.buy, 43.95, 100)
    order_3 = order.Order('c', '28800540', order.OrderType.buy, 58.93, 100)

    orders = [
        order_1,
        order_2,
        order_3
    ]

    amount = ledger.reduce_orders(orders, maximize=True)
    assert amount == 58.93 * 100 + 44.10 * 100 + 43.95 * 100

    amount = ledger.reduce_orders(orders, maximize=False)
    assert amount == 58.93 * 100 + 44.10 * 100 + 43.95 * 100
