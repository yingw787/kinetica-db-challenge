"""Testing limit_order_book.REDUCE_ORDER_REGEX in src/models/limit_order_book.py.

Methods are separated because if a test fails, remaining assertions within same method are not run.
"""

import re

from src.models import limit_order_book


def test_reduce_order_regex_fails_match_with_improper_timestamp():
    """Asserts that limit_order_book.REDUCE_ORDER_REGEX fails match with improper timestamp.

    Timestamps are integers between 0 and 9 inclusive.
    """
    entry = '2880074A R b 100\n'

    assert re.match(limit_order_book.REDUCE_ORDER_REGEX, entry) is None


def test_reduce_order_regex_fails_match_with_improper_buy_command():
    """Asserts that limit_order_book.REDUCE_ORDER_REGEX fails match without a buy command.

    Buy command is indicated with a 'B'.
    """
    entry = '28800744 Q b 100\n'

    assert re.match(limit_order_book.REDUCE_ORDER_REGEX, entry) is None


def test_reduce_order_regex_fails_match_with_improper_order_id():
    """Asserts that limit_order_book.REDUCE_ORDER_REGEX fails match with an improper order ID.

    Order IDs are assumed to be lowercase characters from the alphabet.
    """
    entry = '28800744 R !12 100\n'

    assert re.match(limit_order_book.REDUCE_ORDER_REGEX, entry) is None


def test_reduce_order_regex_fails_match_with_improper_number_of_shares():
    """Asserts that limit_order_book.REDUCE_ORDER_REGEX fails match with an improper number of shares.

    Number of shares is assumed to be an integer value.
    """
    entry = '28800744 R b 100.00\n'

    assert re.match(limit_order_book.REDUCE_ORDER_REGEX, entry) is None


def test_reduce_order_regex_succeeds_with_valid_entry():
    """Asserts that limit_order_book.REDUCE_ORDER_REGEX matches a valid entry.
    """
    entry = '28800744 R b 100\n'

    assert re.match(limit_order_book.REDUCE_ORDER_REGEX, entry) is not None
