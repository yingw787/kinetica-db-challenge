"""Regression test and helpers to compare given pricer.out.200 and generated output from pricer.in for target size of 200.
"""

import filecmp
import os

import config
from src.models import pricer


def test_pricer_example_200():
    """Wrapper function.
    """
    file_input_path = os.path.abspath(os.path.abspath(config.ROOT_DIR) + '/samples/pricer.in')
    target_size = 200
    file_output_path = os.path.abspath(os.path.abspath(config.ROOT_DIR) + '/tests/regression/pricer.generated_out.200')
    test_pricer_instance = pricer.Pricer(file_input_path, target_size, file_output_path, progressbar=True)

    test_pricer_instance.execute()

    baseline = os.path.abspath(os.path.abspath(config.ROOT_DIR) + '/samples/pricer.out.200')

    assert filecmp.cmp(baseline, file_output_path)

    os.remove(file_output_path)
