### Kinetica DB Code Challenge

#### Getting Started

1. Ensure that you have [`pipenv`](https://docs.pipenv.org/) installed. I built and tested this using Python 2.7 and pipenv 9.0.1.
2. In `bash`, `git clone` the repository `git@gitlab.com:yingw787/` to `/path/to/directory`.
3. `cd` to `/path/to/directory` and run `pipenv install`. This should install all dependencies specified in `Pipfile`.
4. In order to run the example case, run `python pricer.py 200` in the root directory. This will execute the program over `samples/pricer.example.in` for a target size of 200, and log the results to standard output. The output displayed in the terminal should match `samples/pricer.example.out.200`.
5. In order to run the tests, run `py.test tests` from the root directory.
    - In order to run the unit tests, run `py.test tests/unit/`.
    - In order to run a regression test, run `py.test tests/regression/<regression_test_file>`.
        - I would not recommend running all the regression tests at once as they are slow.
        - Regression tests can be run in parallel as a new state store is instantiated for every run.
        - If you would like to see a progress bar show up, use the flags `--show-progress` and `-s`, e.g., `py.test --show-progress tests/regression/test_pricer_1.py -s`.

#### Synopsis of Problem

- A log file contains three types of entries:
    - limit orders to buy, called "bids"
    - limit orders to sell, called "asks"
    - reductions in orders already logged
- Solution must output changes to the maximum income generated from bids, or the minimum expense generated from asks.

#### Observations
- Every single entry in the log file must be touched at least once in order to ensure that the total income/expense generated will be accurate.
- Orders must be preserved across lines in the log file. This is from the need to address "Reduce Order" messages and from the accumulation of share/limit price associations, among others.
- The order of the log file entries matter in computing the total income/expense. However, since every log file entry has a timestamp that is (presumably) accurate, the entries do not necessarily need to be stored in the order they are read.
- During testing, invalid log file entries may be present, and must be handled by the pricer.
- The order of the orders in the limit order book *during accumulation* is extremely important, because that is how you maximize the target size income / minimize the target size expense.
- At the same time, having accesses to orders be cheap is very important for reducing orders.

#### Data Structures Considered
- Linked List: Insertions would be O(1), and sorted status could be maintained across insertions. However, search is O(N) as elements are not indexed.
- Heap / Priority Queue: Insertions would be O(1), and re-balancing would occur in O(log N) time. However, search for a random given element in a heap is O(N), as elements cannot be accessed by index.
- Array: Access by element is O(1); however, insertion and deletion are O(N).
- Dictionary / Associative Array: Insertions and deletions are O(1), and search is O(1).

Ended up going with dictionaries as state store.

#### Methodologies toward Solution
- If data needs to be preserved, then the solution must be stateful. Model the problem using classes/objects and classical design patterns.
- Use regular expressions for validating each log file entry.
    - Entries can be of two different types: `timestamp A order-id side price size`, or `timestamp R order-id size`.
    - All other input cases should be treated as error conditions.
    - Regular expressions are more performant than string manipulation.
- The program should fail fast should there be any errors. That way the problem can be readily fixed.
- Interfaces should be flexible and composable. For example, the state store should be different from the file I/O and have independent interfaces.

#### Solution Description

- Read the log file, and parse the inputs.
    - If the entry is a bid, and the number of shares is greater than or equal to the target size, then sort the bids by lowest first, then reduce the shares until the number of shares matches the target size. If it is different from the stored value, then print the result.
    - If the entry is an ask, and the number of shares is greater than or equal to the target size, then sort the asks by highest first, then reduce the asks from greatest to least until the number of shares matches the target size. If it is different from the stored value, then print the result.
    - If the entry is a reduction, reduce the shares, eliminate orders if applicable, and recalculate the total income/expense. If the number of shares total is less than the target size, return 'NA'.

#### Design Choices

- I thought I might need to create an abstract class and concrete classes in order to read different inputs (e.g. from standard input or from file), but the standard library contains most all the functionality and the utility my own classes would provide would be negligible.
- I thought about using `@staticmethod` in `limit_order_book.LimitOrderBook` (for example, passing around the target size as a function input), but decided against it. It would not make testing any easier, and keeping the methods within the class definition makes understanding the class more intuitive.

#### Answers to questions

1. How did you choose your implementation language?

    I chose Python to be my implementation language, for a number of reasons. The primary reason is that Python is my day-to-day programming language and the language I am most familiar with. Additionally, I like Python because of its fast prototyping capability, its rich library ecosystem, and good object-oriented support.

2. What is the time complexity for processing an Add Order message?

    Along the critical path, the time complexity of processing an Add Order message is **O(N log N)**, where N is the number of currently existing entries:
        - `LimitOrderBook().update()` takes O(1) time, as it is a regular expression comparison and routing.
        - Both `LimitOrderBook().handle_bid()` and `LimitOrderBook.handle_ask()` take O(N log N) time.
            - `LimitOrderBook().parse_entry_to_order()` takes O(1) time, as it strips and formats a string and instantiates an Order object.
            - `LimitOrderBook().reduce_orders()` takes O(N log N) time.
                - Sorting a list of N orders takes O(N log N) time in the worst case.
                - For loop accumulation takes O(N) time.
                - Upper bound of method is O(N log N).
            - Comparison conditional takes O(1) time.
            - Conditional logging takes O(1) time.
        - In the worst case, all entries are bids or asks, and the next entry will be the same; therefore, `LimitOrderBook().reduce_orders()` must process all existing entries.

3. What is the time complexity for processing a Reduce Order message?

    Along the critical path, the time complexity of processing a Reduce Order message is **O(N log N)**, whre N is the number of currently existing entries:
        - `LimitOrderBook().update()` takes O(1) time, as it is a regular expression comparison and routing.
        - `LimitOrderBook().handle_reduction()` takes O(N log N) time.
            - `_parse_entry()` takes O(1) time
            - Both `_reduce_bids()` and `_reduce_asks()` take O(1) time
            - Both `_update_target_size_expense()` and `_update_target_size_income()` take O(N log N) time, as they depend on `LimitOrderBook().reduce_orders()`.

4. If your implementation were put into production and found to be too slow, what ideas would you try out to improve its performance? (Other than reimplementing it in a different language such as C or C++.)

    I would try seeing if I could split processing bids and asks into two separate processes. I note that the maximum target size income is dependent only on bids, and that the minimum target size expense is dependent only on asks. Therefore, entries can be routed to a process based on whether the entry affects bids/asks. With both processes running in parallel, the time could in the best case be halved (assuming even divide in entries between bids and asks, and same for any reductions).

    If the program must be run multiple times over the same file, another option could be to add metadata to the log file by passing it through a pre-processor. The metadata would be the accumulated maximum target size income and minimum target size expense. In this case, the pass through the pre-processor would be the same time and additional space complexity (maybe more since interfacing with files is required). However, if multiple passes were required, this time would be amortized as every additional pass would be O(1) for processing individual Add/Reduce Order messages.

    Following the prior statement, another optimization that could be made would be to convert the type of the limit price from floating point to integer. I encountered inaccuracies in adding floating point values over large inputs, and had to round the value to two decimal places. Using integers should prevent that from happening and could save a few instructions per entry.
