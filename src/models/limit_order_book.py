"""Classes, functions, and variables to support LimitOrderBook.
"""

import re

from src.models import order as order_model

# Regular expressions are constant, so do not tie in with class definition.
# Regular expressions are constructed as a series of capturing groups, tested on entries for pricer.example.in.
ADD_BID_REGEX = '^([0-9]*) (A) ([a-z]*) (B) (\d+\.\d+) (\d*)\\n$'
ADD_ASK_REGEX = '^([0-9]*) (A) ([a-z]*) (S) (\d+\.\d+) (\d*)\\n$'
REDUCE_ORDER_REGEX = '^([0-9]*) (R) ([a-z]*) (\d*)\\n$'


class LimitOrderBook(object):
    """Set of all standing bids and asks.
    """
    def __init__(self, target_size, file_output_path):
        """Instantiation of LimitOrderBook.

        Creates separate stores for bids and asks.

        :param int target_size: Target number of shares.
        :param str file_output_path: Path of the file to write output to. If this is None, output will be written to standard output.
        """
        self.target_size = target_size
        self.bids = {}
        self.asks = {}
        self.target_size_income = 'NA'
        self.target_size_expense = 'NA'
        self.number_of_bid_shares = 0
        self.number_of_ask_shares = 0
        self.file_output_path = file_output_path

    def update(self, entry):
        """Updates instance of LimitOrderBook with new entry.

        Validates the entry in the log file.

        :param str entry: Entry in the log file.
        :raises AssertionError: If entry does not satisfy regular expressions.
        """
        # If entry is bid:
        if re.match(ADD_BID_REGEX, entry):
            self.handle_bid(entry)
        # If entry is ask:
        elif re.match(ADD_ASK_REGEX, entry):
            self.handle_ask(entry)
        # If entry is reduce:
        elif re.match(REDUCE_ORDER_REGEX, entry):
            self.handle_reduction(entry)
        # If entry is none of the above, it is invalid.
        else:
            raise AssertionError('Invalid entry')

    @staticmethod
    def parse_entry_to_order(entry, order_type):
        """Parses an entry into an order.

        :param str entry:
        :param enum order_type: from src.models.order.OrderType.
        """
        params = entry.rstrip('\n').split(' ')

        order = order_model.Order(
            params[2],
            int(params[0]),
            order_type,
            float(params[4]),
            int(params[5])
        )

        return order

    def handle_bid(self, entry):
        """Handles a bid.

        Logs output to a file or to standard output.

        :param str entry:
        """
        bid = self.parse_entry_to_order(entry, order_model.OrderType.buy)
        self.bids[bid.id] = bid
        self.number_of_bid_shares += bid.number_of_shares

        if self.number_of_bid_shares >= self.target_size:
            income = self.reduce_orders(self.bids.values(), True)

            if self.target_size_income != income:
                self.target_size_income = income
                log_entry = str(bid.timestamp) + ' S ' + '{:0.2f}'.format(income)
                self.log_output(log_entry)

    def handle_ask(self, entry):
        """Handles an ask.

        Logs output to a file or to standard output.

        :param str entry:
        """
        ask = self.parse_entry_to_order(entry, order_model.OrderType.sell)
        self.asks[ask.id] = ask
        self.number_of_ask_shares += ask.number_of_shares

        if self.number_of_ask_shares >= self.target_size:
            expense = self.reduce_orders(self.asks.values(), False)

            if self.target_size_expense != expense:
                self.target_size_expense = expense
                log_entry = str(ask.timestamp) + ' B ' + '{:0.2f}'.format(expense)
                self.log_output(log_entry)

    def handle_reduction(self, entry):
        """Handles a reduction.

        Logs output to a file or to standard output.

        :param str entry:
        """
        def _parse_entry(entry):
            """Parses an entry to get usable values to reduce LimitOrderBook.

            :param str entry:
            :raises ValueError: If the order ID is not present in the limit order book.
            :returns: tuple (timestamp, order_id, number_of_shares)
                WHERE
                str timestamp is the timestamp of the log entry.
                str order_id is the ID of the order to be reduced.
                int number_of_shares is the number of shares to be cut down.
            """
            params = entry.rstrip('\n').split(' ')

            timestamp = params[0]
            order_id = params[2]
            number_of_shares = int(params[3])

            return timestamp, order_id, number_of_shares

        def _reduce_bids(timestamp, order_id, number_of_shares):
            """Apply reduction to bids.

            :param str timestamp:
            :param str order_id:
            :param int number_of_shares:
            """
            if self.bids[order_id].number_of_shares <= number_of_shares:
                del self.bids[order_id]
            else:
                bid = self.bids[order_id]
                bid.number_of_shares -= number_of_shares
                self.bids[order_id] = bid
            self.number_of_bid_shares = max(self.number_of_bid_shares - number_of_shares, 0)

        def _reduce_asks(timestamp, order_id, number_of_shares):
            """Apply reduction to asks.

            :param str timestamp:
            :param str order_id:
            :param int number_of_shares:
            """
            if self.asks[order_id].number_of_shares <= number_of_shares:
                del self.asks[order_id]
            else:
                ask = self.asks[order_id]
                ask.number_of_shares -= number_of_shares
                self.asks[order_id] = ask
            self.number_of_ask_shares = max(self.number_of_ask_shares - number_of_shares, 0)

        def _update_target_size_expense(timestamp):
            """Update the minimum expense.

            :param str timestamp:
            """
            if self.number_of_ask_shares < self.target_size and self.target_size_expense != 'NA':
                self.target_size_expense = 'NA'
                self.log_output(timestamp + ' B ' + self.target_size_expense)
            elif self.number_of_ask_shares >= self.target_size:
                expense = self.reduce_orders(self.asks.values(), False)
                if self.target_size_expense != expense:
                    self.target_size_expense = expense
                    log_entry = str(timestamp) + ' B ' + '{:0.2f}'.format(expense)
                    self.log_output(log_entry)

        def _update_target_size_income(timestamp):
            """Update the maximum income.

            :param str timestamp:
            """
            if self.number_of_bid_shares < self.target_size and self.target_size_income != 'NA':
                self.target_size_income = 'NA'
                self.log_output(timestamp + ' S ' + self.target_size_income)
            elif self.number_of_bid_shares >= self.target_size:
                income = self.reduce_orders(self.bids.values(), True)
                if self.target_size_income != income:
                    self.target_size_income = income
                    log_entry = str(timestamp) + ' S ' + '{:0.2f}'.format(income)
                    self.log_output(log_entry)

        timestamp, order_id, number_of_shares = _parse_entry(entry)

        if order_id in self.bids:
            _reduce_bids(timestamp, order_id, number_of_shares)
            _update_target_size_income(timestamp)
        elif order_id in self.asks:
            _reduce_asks(timestamp, order_id, number_of_shares)
            _update_target_size_expense(timestamp)
        else:
            raise ValueError('order_id is not present among current bids or asks.')

    def reduce_orders(self, orders, maximize):
        """Reduces a number of orders to generate either the maximum income for bids, or the minimum expense for asks, depending on whether the result should be maximized.

        :param list orders: List of orders, stored as either self.bids.values() or self.asks.values().
        :param bool maximize: Result should be maximized. If `True`, the result will accumulate from greatest to least. If `False`, the result will accumulate from least to greatest.
        :returns: Income or expense.
        :rtype: float
        """
        orders.sort(
            key=lambda order: order.limit_price,
            reverse=maximize
        )

        current_outstanding_shares = self.target_size
        current_accumulated = 0

        for _, order in enumerate(orders):
            if current_outstanding_shares > order.number_of_shares:
                current_accumulated += order.limit_price * order.number_of_shares
                current_outstanding_shares -= order.number_of_shares
            else:
                current_accumulated += order.limit_price * current_outstanding_shares
                break

        return round(current_accumulated, 2)

    def log_output(self, entry_to_log):
        """If file output path is set, log to a particular file. Otherwise, log to standard output.

        The file output path is primarily to make regression testing easier.
        """
        if self.file_output_path:
            file = open(self.file_output_path, 'a')
            file.write(entry_to_log + '\n')
            file.close()
        else:
            print entry_to_log
