"""Classes, functions, and variables to support Order.
"""

import enum


class OrderType(enum.Enum):
    """Type of the Order.
    """
    buy = 'buy'
    sell = 'sell'


class Order(object):
    """Atomic unit of an order placed in the ledger.
    """
    def __init__(self, id, timestamp, order_type, limit_price, number_of_shares):
        """Instantiation of Order.

        :param str id: ID of the order.
        :param int timestamp: milliseconds since midnight.
        :param enum order_type: Type of the order.
        :param float limit_price: maximum price to pay for shares if buying, or minimum price for selling.
        :param int number_of_shares: Number of shares.
        """
        self.id = id
        self.timestamp = timestamp
        self.order_type = order_type
        self.limit_price = limit_price
        self.number_of_shares = number_of_shares
