"""Classes, functions, and variables to support Pricer.
"""

from tqdm import tqdm

from src.models import limit_order_book


class Pricer(object):
    """Top-level Command object that coordinates input reading, the limit order book, and output generation.
    """
    def __init__(self, file_input_path, target_size, file_output_path=None, progressbar=False):
        """Instantiation of Pricer.

        :param str file_input_path:
        :param int target_size:
        :param str file_output_path:
        :param bool progressbar: If you want to see a progress bar in the standard output. Useful for regressions over large files.
        """
        self.file_input_path = file_input_path
        self.ledger = limit_order_book.LimitOrderBook(target_size, file_output_path)
        self.progressbar = progressbar

    def execute(self):
        """Generates total income received if sold target_size shares, or total expense incurred if bought target_size shares.

        Prints values out to standard output.
        """
        # Open the file in read-only mode.
        file = open(self.file_input_path, 'r')

        # Wrap with progressbar if requested.
        if self.progressbar:
            iterator = tqdm(file, total=sum(1 for line in open(self.file_input_path)))
        else:
            iterator = file

        # For every line in the file:
        for entry in iterator:
            # Update the ledger with the new entry.
            # The ledger will update to standard output if need be.
            self.ledger.update(entry)

        # Close the file.
        file.close()
