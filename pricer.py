"""Main Python file to be run.

Validates command line arguments and passes it into Pricer model, which generates result.
"""

import os
import sys

import config
from src.models import pricer

# Set this to read from a file.
LOG_FILE_PATH = os.path.abspath(os.path.abspath(config.ROOT_DIR) + '/samples/pricer.example.in')

if __name__=='__main__':
    # Parse command line argument, throw Exception if not present or not a positive integer.

    # Pass in LOG_FILE_PATH and command line argument into the Pricer model.

    # Will throw KeyError if not in command line arguments, will throw ValueError if not parseable into int.
    target_size = int(sys.argv[1])

    # Manually throw error if target_size <= 0.
    if target_size < 1:
        raise ValueError('Provided target_size must be greater than 0.')

    pricer.Pricer(LOG_FILE_PATH, target_size).execute()
